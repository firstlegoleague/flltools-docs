# FLLTools

> A tournament management tool for the First Lego League

Welcome to the FLLTools documentation website, please select your language from the sidebar.

Disclaimer: The English version is the up to date version and is leading. The translations are done by volunteers, so they might be out of date. 

# Bugs and security

Did you find a bug or security issue within our software, please send us an email at bugs@flltools.com, we will try to get back to you as soon as possible.