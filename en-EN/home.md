# FLLTools documentation

Welcome to the FLLTools documentation website. The website is a fully renewed compared to the FLLScoring tool, FLLnn website and the FIRST Event Hub. We recommend reading the documentation while setting up the website. 

The FLLTools is fully maintained by volunteers, it might take some time for questions to be answered, requested features to be implemented or bugs to be fixed. 