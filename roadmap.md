# Roadmap

Due to it all being volunteers with limited time, there is no fixed roadmap, but a wishlist with features. There are a couple things that need to be done before certain dates.

## Wishlist features

### Judgemodule
- Be able to input the judge forms in the tool
- Edit the judge forms
- Export the results to OJS

### OMR Judge Module
- Generate the judgeforms for OMR
- Import the judgeforms as PDF
- Process the PDF
- Export to OJS

### OJS Module
- Have all the OJS functionality in the FLLTool

### Schedule Module
- Automatically generate a schedule for each tournament
- Frontend for the schedule
- Method to manually create / add / change things in the schedule

### Advanced Team Management
- Be able that Coaches have limited editing power with their own team

### Judge Media Module
- That coaches are able to upload content such as the powerpoints in advance, to smoothline the judging sessions
- Judges to see their own lanes media
- Coaches to update the files

### Displaymodule
- Implement API calls for the screen
- Create the "app" for the screens
- Implement web interface to design screens
- Implement screen manager

### Buttonmodule
- Implement API calls for the buttons
- Create code for the button

### Livestream module
- Implement API's for livestreaming
- Setup the livestream service
- OBS plugin?

### Sponsormodule
- Be able to upload logo's
- Create spaces on the website to show the logs
- Have the logo's on the scoreboard
- Have the logo's on the exports (if enabled)

### More exports module
- Create a whole lot of exports to make.

### Custom scoresheet / Seasonsmodule
- Be able to create your own scoresheets
- Edit custom scoresheets
- Copy official scoresheets (to edit)
- Able to translate custom scoresheets

### Tournaments selecter
- Implement it

### Refereeresults
- View / Edit scores
- Submit scores
- Restrict access
- 

## Currently in development
These functions are currently in development

### Teams Module
- Be able to edit teams  

### Rounds Module
- Need to be tied to a tournament

### Tournaments module
- Edit / Show modules

## Implemented features

### Settings
- Able to set the settings
- Tournaments

### Tournaments module
- Create tournaments
- List tournaments